interface Score {
	label: string,
	filename: string,
	filenamesPatternSets: string[],
	algorithm: string,
	evaluationResults: { AUC: number, AP: number }
}

interface FileItem {
	version: number,
	id: string,
	stackOperations: string[],
	logicalName: string,
	filename: string,
	noColumns: number,
	noRows: number,
	patterns: {
		filename: string,
		filenameOccurances: string,
		type: string,
		columns: string,
		algorithm: string,
		support: string,
		noPatterns: 450,
		label: string
	}[],
	windowCount: number,
	scores: Score[],
	arff: boolean,
	csv: boolean,
	file: string,
}

type ImageFileItem = FileItem & {
	width: number,
	height: number,
	imageCount: number,
	canalCount: number
}

// the backend tables are very strange i know
interface Table {
	rows: string[][],
	totalSize: number,
	rowsStartingFrom1: string[][]
}

interface Project {
	name: string,
	items: (FileItem | ImageFileItem)[],
	fileItems: (FileItem | ImageFileItem)[]
}

// most of them are string representing numbers wich is dumb
interface Stats {
	max:string, //number
	min: string //number,
	std: string //number
	type: string
	mean: string //number,
	IQ_01: string //number,
	IQ_99: string //number
	noValues: string //number,
	histogram?:string //number list separated by a ,
	noNotNull: string //number,
	attribute: string
	noDistinct: string //number,
	't-distribution':string, //number list separated by a ,
	'histogram-dens':string, //number list separated by a ,
	'histogram-labels':string, //number list separated by a ,
	'support-first-value': string //number : number
	'histogram-dens-labels':string, //number list separated by a ,
	'quantiles-IQ099-IQ100':string, //number list separated by a ,
	'quantiles-IQ000-IQ010':string, //number list separated by a ,
	't-distribution-expected':string, //number list separated by a ,
}
