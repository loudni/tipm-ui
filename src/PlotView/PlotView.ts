import { defineComponent } from 'vue'
import HeaderComponent from '../components/HeaderComponent/HeaderComponent.vue'
import TableComponent from '@/components/TableComponent/TableComponent.vue'
import Multiselect from '@vueform/multiselect'
import ChartComponent, { type BackgroundElement, type ChartElement } from '@/components/ChartComponent/ChartComponent.vue'
import Slider from '@vueform/slider'
import makeTheSliderDragOnLock from './makeTheSliderDragOnLock'
import secureFetch from '@/secureFetch'

//bellow this value the slider has two part wich allow the user to zoom in on the data
const DOUBLE_SLIDER_MAX_VALUE = 100000

export default defineComponent({
	name: 'PlotView',
	components: {
		HeaderComponent,
		TableComponent,
		Multiselect,
		ChartComponent,
		Slider
	},
	data() {
		return {
			dataset: null as Table | null,
			fileItem: null as FileItem | null,
			columns: [] as string[],
			selectedY: [] as string[],
			selectedEvents: [] as string[],
			selectedColumns: [] as string[],
			selectedAnomalyPatterns: [] as string[],
			selectedPatterns: [] as string[],
			selectedPatternSet: "",
			patterns: null as Table | null,
			patternsOcc: null as Table | null,
			scores: {} as Record<string, Table>,
			showedColumns: [] as number[],
			chartScale: [] as Array<string | number>,
			chartData: [] as ChartElement[],
			abortController: new AbortController(),
			showAnomalies: true,
			showWindows: false,
			selectedX: 'step',
			slider: [0, 1000] as number | number[],
			sliderOptions: {
				animate: false
			},
			sliderMax: 1000
		}
	},
	computed: {
		isReady() {
			return true
		},

		hasWindows() {
			return localStorage.getItem('hasWindows') === 'true'
		},

		patternCount(): number {
			return new Set(this.patternsOcc?.rowsStartingFrom1.map(e => e[1])).size
		}
	},

	watch: {
		selectedPatterns() {
			this.loadAnomalyScores()
		},

		selectedColumns() {
			this.generateChartData()
		},

		selectedAnomalyPatterns() {
			this.loadAnomalyScores()
		},

		selectedPatternSet() {
			this.fetchPatternData()
		},

		async slider() {
			if(Array.isArray(this.slider)) {
				let xrangeMax= this.slider[1] || this.slider[0] + 1000
				const xrangeMin= this.slider[0]

				if(xrangeMin > xrangeMax ||
					xrangeMax < 0 || xrangeMin < 0) {
					return
				}

				//the server has a hard limit of 1K
				if(xrangeMax - xrangeMin > 1000) {
					xrangeMax = xrangeMin + 1000
				}
				await this.loadDataSet(xrangeMin, xrangeMax, true)
			} else await this.loadDataSet(this.slider, this.slider + 1000, true)

			this.generateChartData()
		},
		selectedX() {
			this.generateChartData()
		}
	},
	async mounted() {
		if(!localStorage.getItem('datasetId')) {
			this.$router.push('/')
		}

		this.fileItem = await (await secureFetch(this, `${import.meta.env.VITE_BACKEND_URL}/rest/metadata/fileitem?id=${localStorage.getItem('datasetId')}`)).json()

		this.sliderMax = this.fileItem?.noRows || 1000
		if(this.sliderMax < DOUBLE_SLIDER_MAX_VALUE) {
			makeTheSliderDragOnLock(this.sliderMax)
			this.slider = [0, 1000]
		}
		else {
			this.slider = 0
		}
		this.generateChartData()
		this.fetchPatternData()
	},
	updated() {
		if(this.sliderMax < DOUBLE_SLIDER_MAX_VALUE)makeTheSliderDragOnLock(this.sliderMax)
	},
	methods: {
		async loadDataSet(xmin?: number, xmax?: number, cancelPrevious = false) {
			if(cancelPrevious)this.abortController.abort()
			const id = localStorage.getItem('datasetId')
			if(id) {
				this.abortController = new AbortController()
				await secureFetch(this, `${import.meta.env.VITE_BACKEND_URL}/rest/load-data?id=${id}&windowed=false&pagination=${xmin || 0}-${xmax || 1000}`, { signal: this.abortController.signal }).then(async (response) => {
					this.dataset = await response.json()
					this.columns = this.dataset?.rows[0] || []
				}).catch(() => {/**/})
			}
		},

		columnId(label: string): number {
			const index = this.dataset?.rows[0].indexOf(label)
			return index !== undefined ? index: -1
		},

		async loadAnomalyScores() {
			const scores = this.fileItem?.scores.filter(e => this.selectedAnomalyPatterns.includes(e.label))
			if(!scores || !scores.length) {
				this.scores = {}
			} else {
				for(const score of scores) {
					//don't load twice the same data
					if(!this.scores[score.label]) {
						this.scores[score.label] = await (await secureFetch(this, `${import.meta.env.VITE_BACKEND_URL}/rest/load-anomaly-score?filename=${score.filename}&id=${localStorage.getItem('datasetId')}`)).json()
					}
				}
			}

			this.generateChartData()
		},

		async generateChartBackground(chartData: ChartElement[], dataset: Table) {
			const labelColumnId = this.columnId('label')
			if(labelColumnId !== -1 && this.showAnomalies) {
				const labels = this.extractColumn(dataset, labelColumnId)
				chartData.push({
					type: 'Background',
					label: 'anomalies',
					color: ["#FF5555", "#f4fff4"],
					data: labels.map(e => parseFloat(e) !== 1)
				} as BackgroundElement)
			}
		},

		async generateChartBlocks(chartData: ChartElement[], dataset: Table) {
			if(this.hasWindows && this.showWindows)chartData.push({
				type: 'Block',
				data: this.windowToStepIntervals(dataset),
				coloringMode: 'stroke',
				color: [ '#DDD2'],
				label: 'windows'
			})
		},

		async generateChartLine(chartData: ChartElement[], dataset: Table) {
			const colors = [ '#4e93c3', '#ff8a24', '#aec7e8', ...[...Array(dataset.rowsStartingFrom1.length)].map(this.generateRandomColor)]
			for(const [ index, columnName ] of this.selectedColumns.entries()) {
				const values = this.extractColumn(dataset, this.columnId(columnName))
				chartData.push({
					type: 'Line',
					data: values.map(e => parseFloat(e)),
					label: this.dataset?.rows[0][this.columnId(columnName)] || 'no name',
					color: colors[index]
				})
			}
		},

		async generateChartSelfScaledLine(chartData: ChartElement[], dataset: Table) {
			const scores = this.scores
			const windows = this.extractColumn(dataset, this.columnId('Window')).map(e => parseFloat(e))
			if(scores) {
				for(const patternScore of this.selectedAnomalyPatterns) {
					console.log(Object.keys(scores))
					const visibleScore = scores[patternScore].rowsStartingFrom1.filter(e => windows.includes(parseFloat(e[0])))
					chartData.push({
						type: 'SelfScaledLine',
						data: visibleScore.map(e => parseFloat(e[1])),
						max: 1,
						min: 0,
						label: 'outliner',
						color: '#FF0000'
					})
				}
			}
		},

		occurrencesToMap(occurrences: string[][]): Record<string, string[]> {
			const map: Record<string, string[]> = {}
			for(const row of occurrences) {
				const [window, patternId] = row
				if(map[patternId] === undefined) {
					map[patternId] = [ window ]
				} else {
					map[patternId].push(window)
				}
			}
			return map
		},

		async generateChartLabels(chartData: ChartElement[], dataset: Table) {
			const patternOcc = this.patternsOcc
			if(patternOcc && patternOcc.rowsStartingFrom1 && this.selectedPatterns.length) {
				const windowIntervals = this.windowToStepIntervals(dataset)
				//todo search for the window columns
				const firstRow = dataset.rowsStartingFrom1[0]
				const firstWindow = firstRow[firstRow.length - 1]
				const occurrences = this.occurrencesToMap(patternOcc.rowsStartingFrom1)

				for(const [index, patternId] of this.selectedPatterns.entries()) {
					const patternWindows = occurrences[(parseInt(patternId) + 1).toString()]

					//undefined values are values that is outside the current graph
					// so we remove them with a filter
					//TODO include instead of parse
					const windowsPlacements = patternWindows.map(w => windowIntervals[parseInt(w) - parseInt(firstWindow)]).filter(e => !!e)

					chartData.push({
						type: 'Label',
						label: patternId.toString(),
						data: windowsPlacements.map(e => (e.begin + e.end) / 2),
						color: '#FF000055',
						size: 10,
						y: (index + 1) / this.selectedPatterns.length
					})
					console.log((parseInt(patternId) + 1) / this.selectedPatterns.length)
				}
			}
		},

		async generateChartData() {
			this.chartData = []
			this.chartScale = []
			const chartData = [] as ChartElement[]
			const dataset = this.dataset
			if(!dataset) return

			const scaleColumnId = this.columnId(this.selectedX)
			this.chartScale = this.extractColumn(dataset, scaleColumnId).map(e => {
				if(!isNaN(parseFloat(e))) {
					return parseFloat(e)
				}
				return e
			})

			await this.generateChartBackground(chartData, dataset)
			await this.generateChartBlocks(chartData, dataset)
			await this.generateChartLine(chartData, dataset)
			await this.generateChartSelfScaledLine(chartData, dataset)
			await this.generateChartLabels(chartData, dataset)
			this.chartData = chartData
		},

		extractColumn(dataset: Table, col: number) {
			return dataset.rowsStartingFrom1.map(e => e.filter((_val, index) => col === index)).flat()
		},

		windowToStepIntervals(dataset: Table) {
			const windowSets = this.extractColumn(dataset, this.columnId('Window'))

			const windowIntervals = [] as { begin: number, end: number }[]
			for(const [ step, windowSet ] of windowSets.entries()) {
				const windows = windowSet.split(';').map(e => parseInt(e))
				for(const window of windows) {
					if(windowIntervals[window]) {
						windowIntervals[window].end = step
					} else {
						//windows are in order so we don't need to check if the id is right
						windowIntervals.push({ begin: step, end: step })
					}
				}
			}
			return windowIntervals
		},


		generateRandomColor() {
			return `rgb(${Math.round(Math.random()* 255)},${Math.round(Math.random()* 255)},${Math.round(Math.random()* 255)})`
		},

		async fetchPatternData() {
			const patternList = this.fileItem?.patterns
			const selectedPattern = this.selectedPatternSet
			if(!patternList) return
			//simultaneous requests
			let filename = ""
			if(!selectedPattern && patternList[0]) {
				filename = patternList[0].filename
				this.selectedPatternSet = patternList[0].label
			} else if(patternList.length) {
				const patternId = patternList.findIndex(e => e.label === selectedPattern)
				filename = patternList[patternId].filename
			}

			this.patterns = await (await secureFetch(this, `${import.meta.env.VITE_BACKEND_URL}/rest/load-patterns?filename=${encodeURIComponent(filename)}&id=${localStorage.getItem('datasetId')}`)).json()
			this.patternsOcc =  await (await secureFetch(this, `${import.meta.env.VITE_BACKEND_URL}/rest/load-pattern-occ?filename=${encodeURIComponent(filename)}&id=${localStorage.getItem('datasetId')}`)).json()
			this.selectedPatterns = []
		},
		selectAll() {
			this.selectedPatterns = this.patterns?.rowsStartingFrom1.map((_val, index) => index.toString()) || []
		}
	},
})
