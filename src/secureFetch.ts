import type { ComponentPublicInstance } from "vue"



async function secureFetch(self: ComponentPublicInstance, url: string, options?: RequestInit) {
	const promise = fetch(url, options)
	try {
		//await so we can capture exceptions
		return await promise
	} catch(e: any) {
		if(e.name === "AbortError") {
			//if the exception is an abort error we just directly give it back
			return promise
		}
		self.$toast.clear()
		self.$toast.show('Could not contact the server make sure it is running on port 8080', { maxToasts: 1, type: 'error', duration: 3600 * 1000 })
		let serverAvailable = false
		while(!serverAvailable) {
			await new Promise((res) => {
				setTimeout(() => {
					fetch(import.meta.env.VITE_BACKEND_URL, { method: 'HEAD' }).then(() => {
						self.$toast.clear()
						self.$toast.success('The connection to the server is back up :)')
						serverAvailable = true
						res(null)
					}).catch(() => { /* juste ignore exceptions */ res(null) })
				}, 1000)
			})
		}
		return fetch(url, options)
	}
}


export default secureFetch
