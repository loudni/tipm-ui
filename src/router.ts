export default [
	{ path: '/', component: () => import('./Index/Index.vue') },
	{ path: '/table', component: () => import('./TableView/TableView.vue') },
	{ path: '/transform', component: () => import('./TransformView/TransformView.vue') },
	{ path: '/plot', component: () => import('./PlotView/PlotView.vue') },
	{ path: '/mine', component: () => import('./MineView/MineView.vue')},
	{ path: '/image', component: () => import('./ImageMiningView/ImageMiningView.vue')},
	{ path: '/imageView', component: () => import('./ImageView/ImageView.vue')}
]
