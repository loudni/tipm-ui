import { defineComponent } from 'vue'
import HeaderComponent from '../components/HeaderComponent/HeaderComponent.vue'
import secureFetch from '@/secureFetch'
import Multiselect from '@vueform/multiselect'

export default defineComponent({
	name: 'TableView',
	components: {
		HeaderComponent,
		Multiselect
	},
	data() {
		return {
			imageId: null as number | null,
			canalId: null as number | null,
			stats: [] as Stats[],
			imageFileItem: null as ImageFileItem | null,
			imageLegend: null as Record<string, number> | null,
			image: ""
		}
	},
	computed: {
		imageIds(): number[] {
			//map doesn't work right after array[] so we need to fill
			return Array(this.imageFileItem?.imageCount).fill(0).map((_value, index) => index)
		},

		canalIds(): Record<number, string> {
			const values = this.stats
				.filter((val) => val.attribute !== 'Window')

			const result: Record<number, string> = {}
			for(let stat = 0; stat < values.length; stat++) {
				result[stat] = values[stat].attribute
			}

			return result
		},

		isReady(): boolean {
			return !!this.stats.length && !!this.imageFileItem
		}
	},
	watch: {
		imageId() {
			this.updateImage()
		},

		canalId() {
			this.updateImage()
		}
	},
	async mounted(){
		const datasetId = localStorage.getItem('datasetId')
		if(datasetId === null) {
			console.log("no dataset going back...")
			this.$router.back()
			return
		}
		this.stats = await ( await secureFetch(this, `${import.meta.env.VITE_BACKEND_URL}/rest/metadata/attribute-and-statistics?id=${encodeURIComponent(datasetId)}`)).json()
		const fileitem = await (await secureFetch(this, `${import.meta.env.VITE_BACKEND_URL}/rest/metadata/fileitem?id=${encodeURIComponent(datasetId)}`)).json() as ImageFileItem | FileItem

		if("width" in fileitem) {
			this.imageFileItem = fileitem
		} else {
			this.$router.back()
		}
	},
	methods: {
		async updateImage() {
			const dataset = encodeURIComponent(localStorage.getItem('datasetId') || "")
			if(dataset && this.imageId != null && this.canalId != null) {
				this.imageLegend = null
				this.image = URL.createObjectURL(await (await secureFetch(this, `${import.meta.env.VITE_BACKEND_URL}/image/project?id=${dataset}&imageid=${this.imageId}&canalid=${this.canalId}`)).blob())
				this.imageLegend = await (await secureFetch(this, `${import.meta.env.VITE_BACKEND_URL}/image/projectmeta?id=${dataset}&imageid=${this.imageId}&canalid=${this.canalId}`)).json()
			}
		}
	},
})
