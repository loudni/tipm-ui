import { defineComponent, type PropType } from 'vue'

interface BlockElement {
	type: 'Block',
	label: string,
	data: { begin: number, end: number }[]
	coloringMode?: 'fill' | 'stroke',
	color: string[],
}

interface BackgroundElement {
	type: 'Background',
	data?: boolean[],
	label?: string,
	color: string[]
}

interface LineElement {
	type: 'Line',
	label: string,
	data: number[],
	color: string
}

interface SelfScaledLineElement {
	type: 'SelfScaledLine',
	label: string,
	max?: number,
	min?: number
	data: number[],
	color: string
}

interface LabelElement {
	type: 'Label'
	label: string
	data: number[] //list of points
	y?: number // scale between 0 and 1
	size: number // px
	color: string
}

type ChartElement = BlockElement | LineElement | BackgroundElement | SelfScaledLineElement | LabelElement

export type { ChartElement, BlockElement, LineElement, BackgroundElement }

export default defineComponent({
	name: 'ChartComponent',
	props: {
		data: { type: Array as PropType<ChartElement[]>, required: true },
		scale: { type: Array as PropType<Array<number | string>>, required: true },
		width: { type: Number, default: null },
		height: { type: Number, default: null},
		scaleHeight: { type: Number, default: null}
	},
	data() {
		return {
			canvasId: this.$.uid
		}
	},
	watch: {
		data() {
			this.render()
		},
		scale() {
			this.render()
		}
	},
	mounted() {
		this.render()
	},
	methods: {
		render() {
			//@ts-expect-error getElement by id returns only generic types
			const canvas: HTMLCanvasElement = document.getElementById(this.canvasId.toString())
			const height = this.height || 400

			if(!this.hasData()) throw new Error('no data provided')

			const { maxY, minY } = this.computeYScale()

			canvas.width = this.width || canvas.offsetWidth
			canvas.height = height

			const context = canvas.getContext('2d')
			if(!context) {
				throw new Error('No 2D context available')
			}

			//add right padding
			const { scaleHeight, dataGap, width } = this.drawScale(canvas, context)

			for(const element of this.data) {
				switch(element.type) {
					case 'Background': {
						if(!element.data) {
							const color = element.color[0]
							context.save()
							context.fillStyle = color
							context.fillRect(scaleHeight, 0, width - scaleHeight, height)
							context.restore()
						} else {
							for(const [index, data] of element.data.entries()) {
								const color = element.color[data ? 1 : 0]
								context.save()
								context.fillStyle=color
								context.fillRect(index * dataGap + scaleHeight, 0, Math.ceil(dataGap), height - scaleHeight)
								context.restore()
							}
						}
						break
					}
					case 'Line': {
						for(const [ index, data ] of element.data.entries())  {
							const placementRatio = (data - minY) / ( maxY - minY)
							const placementHeight = height - scaleHeight - ( placementRatio * ( height - scaleHeight ) )

							const nextPoint = element.data[index + 1]
							if(nextPoint === undefined || nextPoint === null) {
								break
							}
							const nextPlacementRatio = (nextPoint - minY) / ( maxY - minY)
							const nextPlacementHeight = height - scaleHeight - ( nextPlacementRatio * ( height - scaleHeight ))

							this.drawConnectedPoints(context,
								element.color,
								index * dataGap + scaleHeight,
								placementHeight,
								(index + 1) * dataGap + scaleHeight,
								nextPlacementHeight
							)
						}
						break
					}
					case 'SelfScaledLine': {
						const min = element.min !== undefined ?
							element.min :
							element.data.reduce((prev, cur) => cur > prev ? cur : prev)

						const max = element.max !== undefined ?
							element.max :
							element.data.reduce((prev, cur) => cur < prev ? cur : prev)

						const selfScaledDataGap = ( width - scaleHeight ) / element.data.length

						for(const [ index, data ] of element.data.entries())  {
							const placementRatio = (data - min) / ( max - min)
							const placementHeight = height - scaleHeight - ( placementRatio * ( height - scaleHeight ) )

							const nextPoint = element.data[index + 1]
							if(nextPoint === undefined || nextPoint === null) break
							const nextPlacementRatio = (nextPoint - min) / ( max - min)
							const nextPlacementHeight = height - scaleHeight - nextPlacementRatio * ( height - scaleHeight )

							this.drawConnectedPoints(context,
								element.color,
								index * selfScaledDataGap + scaleHeight,
								placementHeight,
								(index + 1) * selfScaledDataGap + scaleHeight,
								nextPlacementHeight
							)
						}
						break
					}
					case 'Block': {
						for(const [ index, data ] of element.data.entries())  {
							const coloringMode = element.coloringMode || 'fill'
							const color = element.color[index % element.color.length]
							if(data.end > this.scale.length) data.end = this.scale.length

							context.save()
							context.fillStyle = color
							context.strokeStyle = color
							context.rect(data.begin * dataGap + scaleHeight, 0, (data.end - data.begin) * dataGap, height - scaleHeight)
							if(coloringMode === 'fill')
								context.fill()
							else
								context.stroke()

							context.restore()
							if(data.end === this.scale.length) break
						}
						break
					}
					case 'Label': {
						for(const data of element.data) {
							const x = data * dataGap + scaleHeight
							const y = (element.y !== undefined ? element.y : 0.5 ) * (height - scaleHeight - element.size * 2)  + element.size
							context.save()
							context.font = `${element.size * 1.5}px Arial`
							context.beginPath()
							context.ellipse(x, y, element.size, element.size, 0, 0, 2 * Math.PI)
							context.fillStyle = element.color
							context.fill()
							context.fillStyle = "#000"
							context.fillText(element.label, x - element.size/4, y + element.size/4, element.size)
							context.restore()
						}
						break
					}
				}
			}

		},

		drawScale(canvas: HTMLCanvasElement, context: CanvasRenderingContext2D) {
			const sectionCount = 30
			const height = canvas.height
			const scaleHeight = this.scaleHeight || Math.min(height * 0.1, 40)
			const width = canvas.width - scaleHeight
			const barsHeight = Math.floor(scaleHeight / 4)
			const fontSize = Math.floor(scaleHeight / 3)
			const dataGap = (width - scaleHeight) / this.scale.length
			const scaleLabelSeparators = width / 30
			const scale = this.scale.length / sectionCount

			const labels = [ ...Array(Math.floor(this.scale.length / scale))].map((_val, index) => this.scale[Math.floor(index * scale)])

			for(const [index, value] of labels.entries()) {
				context.save()
				context.fillStyle="#5b5b5b"
				context.fillRect(index * scaleLabelSeparators + scaleHeight,
					height - scaleHeight,
					2,
					barsHeight
				)

				context.fillRect(0, height - scaleHeight, width, barsHeight / 4)
				context.restore()


				this.printAngledText(context, Math.PI / 6, value.toLocaleString(), index * scaleLabelSeparators + scaleHeight, height - scaleHeight/2, fontSize)
			}

			return { scaleHeight, sectionCount, dataGap, width }
		},

		hasData() {
			for(const chartElement of this.data) {
				if(chartElement.data) return true
			}
			return false
		},

		printAngledText(context: CanvasRenderingContext2D, rotation: number, text: string, x: number, y: number, fontSize: number) {
			context.save()
			context.font = `${fontSize}px Arial`
			context.translate(x - fontSize / 2, y - fontSize / 2)
			context.rotate(rotation)
			context.fillText(text, fontSize / 2, fontSize / 2)
			context.restore()
		},

		computeYScale() {
			const minmaxMap = this.data.map(e => e.data && e.type === 'Line' ? this.computeDatasetYScale(e.data) : undefined)

			const minmax = this.getFirstNotUndefined(minmaxMap)

			if(!minmax) return { minY: 0, maxY: 0 }

			for(const minmaxElement of minmaxMap) {
				if(minmaxElement && minmaxElement.maxY > minmax.maxY)minmax.maxY = minmaxElement.maxY
				if(minmaxElement && minmaxElement.minY < minmax.minY)minmax.minY = minmaxElement.minY
			}

			//padding
			minmax.maxY += 0.5
			return minmax
		},

		getFirstNotUndefined<T>(array: Array<T | undefined>) : T | undefined {
			for(const element of array) {
				if(element !== undefined) return element
			}
			return undefined
		},

		computeDatasetYScale(dataset: number[]) {
			const maxY = dataset.reduce((val, prev) => val > prev ? val : prev)
			const minY = dataset.reduce((val, prev) => val < prev ? val : prev)
			return { minY, maxY }
		},

		drawConnectedPoints(context: CanvasRenderingContext2D, color: string, x1: number, y1: number, x2: number, y2: number) {
			const radius = 1

			context.save()
			context.fillStyle = color
			context.strokeStyle = color
			context.beginPath()
			context.arc(x1, y1, radius, 0, 2 * Math.PI)
			context.fill()

			context.lineWidth = 1
			context.fillStyle = color
			context.strokeStyle = color
			context.beginPath()
			context.moveTo(x1, y1)
			context.lineTo(x2, y2)
			context.stroke()
			context.restore()
		}
	}
})
