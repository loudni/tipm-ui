import { Chart as ChartJS, Tooltip, BarElement, CategoryScale, LinearScale } from 'chart.js'
import { defineComponent } from 'vue'
import { Bar } from 'vue-chartjs'

import Multiselect from '@vueform/multiselect'
import HeaderComponent from '../components/HeaderComponent/HeaderComponent.vue'

import secureFetch from '@/secureFetch'
import switchToNewestProject from '@/switchToNewestProject'

ChartJS.register(Tooltip, BarElement, CategoryScale, LinearScale)

export default defineComponent({
	name: 'TransformView',
	components: {
		HeaderComponent,
		Multiselect,
		Bar
	},
	data() {
		return {
			PAAValues: null as string[] | null,
			PAAwindow: null as number | null,
			discretizedColumns: null as string[] | null,
			selectedNormalizeColumns: [] as string[],
			selectedDropColumns: [] as string[],
			selectedDateColumns: [] as string[],
			selectedTypeColumns: [] as string[],
			selectedEvolutionColumns: [] as string[],
			columnToRename: "",
			bins: null as number | null,
			useDensity: false,
			columns: [] as string[],
			stats: [] as Stats[],
			selectedFilter: null as string | null,
		}
	},
	computed: {
		isReady(): boolean {
			return !!this.columns.length && !!this.stats.length
		},

		selectedFilterObj(): Stats | null {
			const obj = this.stats.find(e => e.attribute === this.selectedFilter)
			if(obj) return obj
			return null
		}
	},
	async mounted() {
		if(!localStorage.getItem('datasetId')) {
			this.$router.push('/')
		}

		await this.loadStats()
	},
	methods: {
		async paa(e: Event) {
			e.preventDefault()
			//TODO: warning
			if(!this.PAAValues || !this.PAAwindow)return
			const form = new FormData()
			form.append('columns', this.PAAValues?.join(','))
			form.append('window', this.PAAwindow.toString())
			form.append('id', ''+localStorage.getItem('datasetId'))
			await secureFetch(this, import.meta.env.VITE_BACKEND_URL + '/rest/transform/paa', { body: form, method: 'POST' })
			await switchToNewestProject(this)
		},

		async discretize(e: Event) {
			e.preventDefault()
			//TODO: warning
			if(!this.discretizedColumns || !this.bins) return
			const form = new FormData()
			form.append('columns', this.discretizedColumns.join(','))
			form.append('density', this.useDensity.toString())
			form.append('bins', this.bins.toString())
			form.append('id', ''+localStorage.getItem('datasetId'))
			await secureFetch(this, import.meta.env.VITE_BACKEND_URL + '/rest/transform/discretize', { body: form, method: 'POST' })
			await switchToNewestProject(this)
		},

		async filterOutliers(e: Event) {
			e.preventDefault()
			// @ts-expect-error the target is a form
			const form = new FormData(e.target)
			form.append('id', ''+localStorage.getItem('datasetId'))
			await secureFetch(this, import.meta.env.VITE_BACKEND_URL + '/rest/transform/replace-outlier', { method: 'POST', body: form })
			await switchToNewestProject(this)
		},


		async createWindows(e: Event) {
			e.preventDefault()
			// @ts-expect-error the target is a form
			const form = new FormData(e.target)
			form.append('id', ''+localStorage.getItem('datasetId'))
			await secureFetch(this, import.meta.env.VITE_BACKEND_URL + '/rest/transform/make-windows', { method: 'POST', body: form })
			await switchToNewestProject(this)
			localStorage.setItem('hasWindows', 'true')
		},

		async runSQL(e: Event) {
			e.preventDefault()
			//@ts-expect-error the target is a form
			const form = new FormData(e.target)
			form.append('id', '' + localStorage.getItem('datasetId'))
			await secureFetch(this, import.meta.env.VITE_BACKEND_URL + '/rest/transform/run-sql-query', { method: 'POST', body: form })
			await switchToNewestProject(this)
		},

		async normalize(e: Event) {
			e.preventDefault()
			return this.genericTransform('normalize', this.selectedNormalizeColumns)
		},

		async dropColumn(e: Event) {
			e.preventDefault()
			return this.genericTransform('remove', this.selectedDropColumns)
		},

		async dateToMillis(e: Event) {
			e.preventDefault()
			return this.genericTransform('datetime2sec', this.selectedDateColumns)
		},

		async typeToCategorical(e: Event) {
			e.preventDefault()
			return this.genericTransform('2categorical', this.selectedTypeColumns)
		},

		async genericTransform(name: string, columns: string[]) {
			const form = new FormData()
			form.append('id', '' + localStorage.getItem('datasetId'))
			form.append('columns', columns.join(','))
			form.append('transforms', name)
			await secureFetch(this, import.meta.env.VITE_BACKEND_URL + '/rest/transform/transform-columns/', { method: 'POST', body: form })
			await switchToNewestProject(this)
		},

		async runEvolution(e: Event) {
			const columns = this.selectedEvolutionColumns
			if(columns) {
				e.preventDefault()
				const form = new FormData()
				form.append('id', '' + localStorage.getItem('datasetId'))
				form.append('columns', columns.join(','))
				await secureFetch(this, import.meta.env.VITE_BACKEND_URL + '/rest/transform/evolution', { method: 'POST', body: form })
				await switchToNewestProject(this)
			}
		},

		async rename(e: Event) {
			e.preventDefault()
			//@ts-expect-error we know it's a form
			const form = new FormData(e.target)
			form.append('old_name', this.columnToRename)
			form.append('id', '' + localStorage.getItem('datasetId'))
			await secureFetch(this, import.meta.env.VITE_BACKEND_URL + '/rest/transform/rename/', { method: 'POST', body: form })
			await switchToNewestProject(this)
		},

		async loadStats() {
			const response = await secureFetch(this, import.meta.env.VITE_BACKEND_URL + `/rest/metadata/attribute-and-statistics?id=${localStorage.getItem('datasetId')}`)
			const stats: Stats[] = await response.json()
			this.columns = stats.map(e => e.attribute)
			//the real values label are bad and this part of the backend's code scare me.
			this.stats = stats.map(e =>  {
				if(e.type === 'REAL') {
					return { ...e,
						'histogram-labels': e['histogram-labels'].split(',').slice(0, -1).join(',')
					}
				}
				return e
			})
		},
	},
})
