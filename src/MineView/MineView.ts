import { defineComponent } from 'vue'
import HeaderComponent from '../components/HeaderComponent/HeaderComponent.vue'
import TableComponent from '@/components/TableComponent/TableComponent.vue'
import Multiselect from '@vueform/multiselect'
import switchToNewestProject from '@/switchToNewestProject'
import secureFetch from '@/secureFetch'

//TODO: add optionals for other algorithms

export default defineComponent({
	name: 'TransformView',
	components: {
		HeaderComponent,
		TableComponent,
		Multiselect
	},
	data() {
		return {
			columns: [] as string[],
			fileItem: null as FileItem | ImageFileItem | null,
			imagePatternSelect: 0,
			selectedPatternColumns: null as string[] | null,
			selectedSequenceColumns: null as string[] | null,
			patternData: null as { rows: string[][], totalSize: number, rowsStartingFrom1: string[][] } | null,
			selectedPatternFPOF: [] as string[],
			itemsetSupport: null as number | null,
			sequentialSupport: null as number | null,
			dictionary: {} as { [ key:string ]: Set<number> },
			selectedPatternPBAD: [] as string[],
			requiredItems: "",
			newRequiredItems: [] as string[],
			currentColumn: null as string | null,
			arePatternsLoading: false,
			selectedImagePatterns: [] as number[],
			imagePattern: "",
			currentPattern: null as string | null,
			selectedAlgorithm: 'PrefixSpan',
		}
	},
	computed: {
		isReady(): boolean {
			return (this.fileItem !== null) && (this.columns !== null)
		},

		tableData(): Array<string | number>[] {
			if(!this.patternData) return []
			const tableData = this.patternData.rows.map(col => [ col[0], col[1], col[2], !isNaN(parseFloat(col[2])) ? 1 - parseFloat(col[2]) : col[2] ])
			tableData[0] = [ 'Pattern', 'Support', 'Conf. norm', 'Conf. abnorm' ]
			return tableData
		},

		hasWindows() {
			return localStorage.getItem('hasWindows') === 'true'
		},

		nbWindows(): number {
			const fileItem = this.fileItem
			if(fileItem) {
				return fileItem.windowCount
			}

			return 0
		},

		isImage(): boolean {
			const fileItem = this.fileItem
			if(fileItem) {
				return "width" in fileItem
			}
			return false
		},

		isImagePatternLoaded(): boolean {
			return !!this.imagePattern
		}
	},
	watch : {
		selectedImagePatterns() {
			this.fetchPatternImage()
		},

		imagePatternSelect() {
			this.fetchPatternImage()
		}
	},
	async mounted() {
		if(!localStorage.getItem('datasetId') || !this.hasWindows) {
			this.$toast.error('You need to have selected a dataset with windows')
			this.$router.push('/')
		}

		//load
		const data: { [key: string]: string }[] | { timestamp: number, status: number, error: string, path: string } = await (await secureFetch(this, `${import.meta.env.VITE_BACKEND_URL}/rest/metadata/attributes?id=${encodeURIComponent(localStorage.getItem('datasetId') || "")}`)).json()
		if(!data || 'status' in data) {
			localStorage.removeItem('datasetId')
			this.$router.push('/')
			return
		}

		this.columns = data.flatMap(val => Object.keys(val))
		const fileItem = await (await secureFetch(this, `${import.meta.env.VITE_BACKEND_URL}/rest/metadata/fileitem?id=${encodeURIComponent(localStorage.getItem('datasetId') || "")}`)).json()
		this.fileItem = fileItem
		this.dictionary = await(await secureFetch(this, `${import.meta.env.VITE_BACKEND_URL}/rest/mining/dictionary?id=${encodeURIComponent(localStorage.getItem('datasetId') || "")}`)).json()
	},
	methods: {
		async runSetMining(e: Event) {
			e.preventDefault()
			if(!this.selectedPatternColumns || !localStorage.getItem('datasetId'))return
			// @ts-expect-error we know it's a form
			const form = new FormData(e.target)
			form.append('columns', this.selectedPatternColumns.join(','))
			form.append('id', '' + localStorage.getItem('datasetId'))
			this.$toast.info("Clustering started")
			const response = await secureFetch(this, import.meta.env.VITE_BACKEND_URL + '/rest/mining/mine', { method: 'POST', body: form })
			if(response.status === 200) {
				this.$toast.info(await response.text())
				switchToNewestProject(this)
			} else {
				if(response.bodyUsed){
					const data = await response.json()
					this.$toast.error(data.message)
				}else{
					this.$toast.error('An error occurred, check the server logs for more details')
				}
			}
		},

		async runSequenceMining(e: Event) {
			e.preventDefault()
			if(!this.selectedSequenceColumns || !localStorage.getItem('datasetId'))return
			// @ts-expect-error we know it's a form
			const form = new FormData(e.target)
			form.append('columns', this.selectedSequenceColumns.join(','))
			form.append('id', '' + localStorage.getItem('datasetId'))
			this.$toast.info("Clustering started")
			const response = await secureFetch(this, import.meta.env.VITE_BACKEND_URL + '/rest/mining/mine', { method: 'POST', body: form })
			if(response.status === 200) {
				this.$toast.info(await response.text())
				switchToNewestProject(this)
			} else {
				if(response.bodyUsed){
					const data = await response.json()
					this.$toast.error(data.message)
				}else{
					this.$toast.error('An error occurred, check the server logs for more details')
				}
			}
		},

		async fetchPatterns(filename: string) {
			this.arePatternsLoading = true
			this.patternData = null
			this.currentPattern = null
			this.patternData = await (await secureFetch(this, `${import.meta.env.VITE_BACKEND_URL}/rest/load-patterns-metadata?filename=${encodeURIComponent(filename)}&id=${encodeURIComponent(localStorage.getItem('datasetId') || "")}`)).json()
			this.currentPattern = filename
			this.arePatternsLoading = false

			await this.fetchPatternImage()
		},

		async fetchPatternImage() {
			this.imagePattern = ""
			const image = await (await secureFetch(this, `${import.meta.env.VITE_BACKEND_URL}/rest/mining/patternRender?filename=${encodeURIComponent(this.currentPattern || "")}&id=${encodeURIComponent(localStorage.getItem('datasetId') || "")}&imageid=${this.imagePatternSelect}&patternids=${this.selectedImagePatterns.join(',')}`)).blob()
			this.imagePattern = URL.createObjectURL(image)
		},

		async deletePatternSet(filename: string) {
			const form = new FormData()

			form.append('filename', filename)
			form.append('id', '' + localStorage.getItem('datasetId'))

			await secureFetch(this, import.meta.env.VITE_BACKEND_URL + '/rest/mining/remove-patternset', { method: 'POST', body: form })
			this.$router.go(0)
		},

		async filterLength(e: Event) {
			e.preventDefault()
			const currentPattern = this.currentPattern
			if(currentPattern) {
				//@ts-expect-error we know target is a form
				const form = new FormData(e.target)
				form.append('filename', currentPattern)
				form.append('id', '' + localStorage.getItem('datasetId'))
				await secureFetch(this, import.meta.env.VITE_BACKEND_URL + '/rest/mining/filter-length', { method: 'POST', body: form })
				this.$router.go(0)
			} else {
				this.$toast.warning('Please select a pattern')
			}
		},

		async filterSupport(e: Event) {
			e.preventDefault()
			const currentPattern = this.currentPattern
			if(currentPattern) {
				//@ts-expect-error we know target is a form
				const domForm: HTMLFormElement = e.target
				const form = new FormData(domForm)

				const topk = form.get('topk')?.toString()
				if(!topk || isNaN(parseInt(topk)) || parseFloat(topk) < 0) {
					const input = domForm.querySelector('input')
					input?.classList.add('is-invalid')
					return
				}

				form.append('filename', currentPattern)
				form.append('id', '' + localStorage.getItem('datasetId'))
				const response = await secureFetch(this, import.meta.env.VITE_BACKEND_URL + '/rest/mining/filter-support', { method: 'POST', body: form })
				if(response.status === 200)this.$router.go(0)
			} else {
				this.$toast.warning('Please select a pattern')
			}
		},

		async removeRedundant(e: Event) {
			e.preventDefault()
			const currentPattern = this.currentPattern
			if(currentPattern) {
				//@ts-expect-error we know target is a form
				const domForm: HTMLFormElement = e.target
				const form = new FormData(domForm)

				const threshold = form.get('threshold')?.toString()
				if(!threshold || isNaN(parseFloat(threshold)) || parseFloat(threshold) < 0 || parseFloat(threshold) > 1) {
					const input = domForm.querySelector('input')
					input?.classList.add('is-invalid')
					return
				}


				form.append('filename', currentPattern)
				form.append('id', '' + localStorage.getItem('datasetId'))
				await secureFetch(this, import.meta.env.VITE_BACKEND_URL + '/rest/mining/filter-jaccard', { method: 'POST', body: form })
				this.$router.go(0)
			} else {
				this.$toast.warning('Please select a pattern')
			}
		},

		async filterGapSpan(e: Event) {
			e.preventDefault()
			const currentPattern = this.currentPattern
			if(currentPattern) {
				//@ts-expect-error we know target is a form
				const domForm: HTMLFormElement = e.target
				const form = new FormData(domForm)

				const value = form.get('maxspan')?.toString() || form.get('maxgap')?.toString()
				if(!value || isNaN(parseInt(value)) || parseFloat(value) < 0) {
					const input = domForm.querySelector('input')
					input?.classList.add('is-invalid')
					return
				}

				form.append('filename', currentPattern)
				form.append('id', '' + localStorage.getItem('datasetId'))
				await secureFetch(this, import.meta.env.VITE_BACKEND_URL + '/rest/mining/filter-time-constraint', { method: 'POST', body: form })
				this.$router.go(0)
			} else {
				this.$toast.warning('Please select a pattern')
			}
		},

		async FrequentPatternOutliersFactor(e: Event) {
			e.preventDefault()
			const form = new FormData()
			form.append('id', '' + localStorage.getItem('datasetId'))
			form.append('patternFilenames', this.selectedPatternFPOF.map(label => this.fileItem?.patterns.find(pattern => pattern.label === label)?.filename).join(','))

			const response = await secureFetch(this, import.meta.env.VITE_BACKEND_URL + '/rest/mining/anomaly-detection-fpof', { method: 'POST', body: form })

			if(response.status === 200) {
				this.$toast.info(await response.text())
			} else {
				if(response.bodyUsed){
					const data = await response.json()
					this.$toast.error(data.message)
				}else{
					this.$toast.error('An error occurred, check the server logs for more details')
				}
			}
		},

		async PatternBasedAnomalyDetection(e: Event) {
			e.preventDefault()
			console.log(this.selectedPatternPBAD.map(label => this.fileItem?.patterns.find(pattern => pattern.label === label)?.filename).join(','))
			const form = new FormData()
			form.append('id', '' + localStorage.getItem('datasetId'))
			form.append('patternFilenames', this.selectedPatternPBAD.map(label => this.fileItem?.patterns.find(pattern => pattern.label === label)?.filename).join(','))

			const response = await secureFetch(this, import.meta.env.VITE_BACKEND_URL + '/rest/mining/anomaly-detection-pbad', { method: 'POST', body: form })

			if(response.status === 200) {
				this.$toast.info(await response.text())
			} else {
				if(response.bodyUsed){
					const data = await response.json()
					this.$toast.error(data.message)
				}else{
					this.$toast.error('An error occurred, check the server logs for more details')
				}
			}
		},

		async filterMinMaxSupport(e: Event) {
			e.preventDefault()

			const currentPattern = this.currentPattern

			if(!currentPattern) {
				this.$toast.error('Please select a pattern')
				return
			}

			//@ts-expect-error we know the target is a form
			const form = new FormData(e.target)
			form.append('id', '' + localStorage.getItem('datasetId'))
			form.append('patternFilename', currentPattern)
			const response = await secureFetch(this, import.meta.env.VITE_BACKEND_URL + '/rest/mining/min-max-filter-support', { method: 'POST', body: form })

			if(response.status === 200) {
				this.$router.go(0)
			} else {
				this.$toast.error(await response.text())
			}
		},

		addRequiredItems(e: Event) {
			e.preventDefault()
			const idsToIgnore: string[] = []

			for(const pairs of this.requiredItems.split(',')) {
				const [column, val] = pairs.split('=')
				if(column !== this.currentColumn) continue

				for(const item of this.newRequiredItems) {
					if(item === val) idsToIgnore.push(item)
				}
			}

			for(const item of this.newRequiredItems) {
				if(!idsToIgnore.includes(item)) {
					if(this.requiredItems)
						this.requiredItems += `,${this.currentColumn}=${item}`
					else
						this.requiredItems = `${this.currentColumn}=${item}`
				}
			}

			this.currentColumn = ""
			this.newRequiredItems = []
		},

		async removeNonEvolving(e: Event) {
			e.preventDefault()

			const currentPattern = this.currentPattern

			if(!currentPattern) {
				this.$toast.error('Please select a pattern')
				return
			}

			// @ts-expect-error we know it's a form
			const form = new FormData(e.target)
			form.append('id', '' + localStorage.getItem('datasetId'))
			form.append('patternFilename', currentPattern)
			const response = await secureFetch(this, import.meta.env.VITE_BACKEND_URL + '/rest/mining/remove-non-evolving-patterns', { method: 'POST', body: form })

			if(response.status === 200) {
				this.$router.go(0)
			} else {
				this.$toast.error(await response.text())
			}
		},

		async mtlClustering(e: Event) {
			e.preventDefault()
			console.log("test")

			const currentPattern = this.currentPattern

			if(!currentPattern) {
				this.$toast.error('Please select a pattern')
				return
			}

			// @ts-expect-error we know it's a form
			const form = new FormData(e.target)
			form.append('id', '' + localStorage.getItem('datasetId'))
			form.append('filename', currentPattern)

			this.$toast.info("Clustering started")
			const response = await secureFetch(this, import.meta.env.VITE_BACKEND_URL + '/image/mtlClustering', { method: 'POST', body: form })
			if(response.status == 200) {
				this.$toast.success("Done")
				const result = await response.json() as { minFreqVal: number, clustering: number[] }
				this.selectedImagePatterns = result.clustering
			} else {
				this.$toast.warning("No solution found")
			}

		}
	},
})
